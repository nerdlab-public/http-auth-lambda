import base64

def lambda_handler(event, context):
    # Get request and request headers
    request = event['Records'][0]['cf']['request']
    headers = request['headers']
 
    auth_user = 'usuario'
    auth_pass = 'clave'
 
    auth_string = 'Basic ' + base64.b64encode((auth_user + ':' + auth_pass).encode()).decode()
 
    if 'authorization' not in headers or headers['authorization'][0]['value'] != auth_string:
        body = 'Unauthorized'
        response = {
            'status': '401',
            'statusDescription': 'Unauthorized',
            'body': body,
            'headers': {
                'www-authenticate': [{'key': 'WWW-Authenticate', 'value': 'Basic'}]
            },
        }
        return response
 
    return request
